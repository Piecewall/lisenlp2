/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lisen2;

/**
 *
 * @author Orlando
 */
public class Lisen2 {
protected Nodo2 inicio2, fin2;
    
    public Lisen2(){
        
        inicio2 = null;
        fin2    = null;
    }

    public static void main(String[] args) {
        Lisen2 listaenlazada2 = new Lisen2();
        listaenlazada2.agregarAlInicioString("Alexander");
        listaenlazada2.agregarAlInicioString("Yazuri");
        listaenlazada2.agregarAlInicioString("William");
        listaenlazada2.agregarAlInicioString("Rodrigo");
        listaenlazada2.agregarAlInicioString("Ricardo");
        listaenlazada2.agregarAlInicioString("Monica");
        listaenlazada2.agregarAlInicioString("Josue");
        listaenlazada2.agregarAlInicioString("Gustavo");
        listaenlazada2.agregarAlInicioString("Eduardo");
        listaenlazada2.agregarAlInicioString("Cristian");
        listaenlazada2.mostrarListaEnlazadaS();
        
    }
     public void agregarAlInicioString(String elemento2){
        inicio2 = new Nodo2(elemento2, inicio2);
        if (fin2 == null){
            fin2 = inicio2;
        }
    }
     public void mostrarListaEnlazadaS(){
         Nodo2 recorrer = inicio2;
         System.out.println("");
         while (recorrer != null){
            System.out.print("["+ recorrer.dato2+"] -->");
            recorrer = recorrer.siguiente2;
        }
        System.out.println("");
    }
        

}
